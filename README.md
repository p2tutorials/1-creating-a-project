# Create a project

Let's get started with our project. Throughout these tutorials we're going to be building an RSS Reader for Jira (because - well why not?).

##Create the actual maven project
To create the project, simply open a command line interface and execute `atlas-create-jira-plugin`. The command will guide you through a series of questions (5 of them in fact). If you're familiar with maven, these will be pretty familiar for you. If not, see below for a brief explanation:

* `Define value for groupId`: The groupId is one part of a maven coordinate(for more info about maven coordinates - see [https://maven.apache.org/pom.html#Maven_Coordinates]()), by default this is a domain name but in reverse. For example - for somebody that owns example.com - this would be com.example.
* `Define value for artifactId`:  The artifactId is the second part of a maven coordinate. Use something like `rssreader-jira`
* `Define value for version`: This is the final maven coordinate as well as the initial version of your add-on. This can be anything you want - **BUT** if you're going to deploy it a real Jira instance - you'll appreciate it if you use [Semantic Versions](semver.org).
* `Define value for package`: This is the java package that should be created. By default it's the groupId.

The groupId and artifactId will be default become your pluginKey. You'll want this to be unique across the Atlassian ecosystem.

![setup.png](setup.png)

Finally the SDK will prompt you to verify the values, if they look good to you, say yes. The SDK will do some magic and create a folder (named the same as the artifactId value). Go ahead and look around inside of it. You'll find the a bunch of files in there:

![folder.png](folder.png)

This is a standard Maven project where:

* src/main/java contains the java code
* src/main/resources contains the non-java code
* src/test contains the unit tests (and integration tests in this case).
* pom.xml is the dependency descriptor (and so much more). For more info see [https://maven.apache.org/pom.html]()

###However there are some important files

* src/main/resources/atlassian-plugin.xml - tells the host product which services this add-on provides as well as which services it depends on (simplifying the importance of this file - it's really important). To see more about it - check out [https://developer.atlassian.com/docs/getting-started/configuring-the-plugin-descriptor/atlassian-plugin-xml-element-reference]().
* src/main/META-INF/spring/plugin-context.xml - this is an internally used file for Spring DM. We'll cover this in a later tutorial when we talk about creating your own Service.
* src/main/resources/jira-rssreader.properties - this is the localization file. Again, we'll talk about this in a later tutorial (in this case translations).

### Next steps

Go ahead and execute `atlas-run` in the directory that contains the pom.xml. This will start Jira up and install the add-on into it. For any tutorial after this we'll assume that this is running each time.

In addition to this, if you're using an IDE - go ahead and import the project into it.

Then head onto the next tutorial.
